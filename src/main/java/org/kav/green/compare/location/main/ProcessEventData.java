package org.kav.green.compare.location.main;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.kav.green.compare.location.config.ConfigProperty;
import org.kav.green.compare.location.model.EventDataElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.javatuples.Pair;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Service
@Slf4j
public class ProcessEventData {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ConfigProperty configProperty;

    public void filterByLocation() throws Exception {
        String eventDataLocation = configProperty.getEventDataLocation();
        log.info("Read files from: {}", eventDataLocation);

        try {
            if (!Files.isDirectory(Paths.get(eventDataLocation))) {
                Files.createDirectory(Paths.get(eventDataLocation));
                log.error("Given directory: {} is empty!", eventDataLocation);
                return;
            }

            List<File> files = Files.walk(Paths.get(eventDataLocation))
                                    .filter(Files::isRegularFile)
                                    .map(Path::toFile)
                                    .collect(Collectors.toList());

            if (files.isEmpty()) {
                log.error("There is no files in directory: {}", eventDataLocation);
                return;
            }

            Map<String, Pair<List<String>, List<String>>> report = new HashMap<>();
            Map<String, Integer> locationCountByfile = new HashMap<>();
            List<String> columnNames = new ArrayList<>(files.size());
            for (File inputFile : files) {
                columnNames.add(inputFile.getName());
                EventDataElement[] inputFileTypeElements = objectMapper.readValue(inputFile, EventDataElement[].class);
                log.info("There are {} elements in file: {}", inputFileTypeElements.length, inputFile.getName());

                for (EventDataElement subjectElement : inputFileTypeElements) {
                    log.info("time: {}, type: {}", subjectElement.getTimestamp(), subjectElement.getSubjectType());
                    String type = subjectElement.getSubjectType();
                    String key = null;
                    if (type.equalsIgnoreCase("LOCATION") && subjectElement.getIdentityType().equalsIgnoreCase("MSISDN")) {
                        Instant instant = Instant.ofEpochMilli(subjectElement.getTimestamp());
                        LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, ZoneOffset.UTC);
                        String formattedDate = localDateTime.format(FileProcessor.formatter);
                        key = subjectElement.getTimestamp() + ";" + formattedDate + ";" + subjectElement.getIdentityType() + ";" + type;
                        FileProcessor.incrementRecordCount(inputFile.getName(), locationCountByfile);
                    }
                    else {
                        continue;
                    }

                    Pair<List<String>, List<String>> fileNamesPair = report.get(key);
                    List<String> fileNames;
                    List<String> inputFileNames = null;
                    if (fileNamesPair == null) {
                        fileNames = new ArrayList<>();
                    }
                    else {
                        fileNames = fileNamesPair.getValue1();
                        inputFileNames = fileNamesPair.getValue0();
                    }

                    fileNames.add(inputFile.getName());
                    fileNamesPair = new Pair<>(fileNames, inputFileNames);
                    report.put(key, fileNamesPair);
                }
            }

            for (String key : report.keySet()) {
                List<String> originalDataFiles = FileProcessor.findInputFiles(key, configProperty.getInputFileLocation());
                Pair<List<String>, List<String>> dataCollection = report.get(key);
                dataCollection = new Pair<>(dataCollection.getValue0(), originalDataFiles);
                report.put(key, dataCollection);
            }

            locationCountByfile.forEach((s, integer) -> {
                log.info("file: {}, location count: {}", s, integer);
            });

            FileProcessor.printReport(columnNames, report, configProperty.getComparisonOutputFileLocation());
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
