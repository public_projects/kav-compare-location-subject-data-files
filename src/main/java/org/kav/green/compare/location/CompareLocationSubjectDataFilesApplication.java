package org.kav.green.compare.location;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.kav.green.compare.location.config.ConfigProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;


@SpringBootApplication
public class CompareLocationSubjectDataFilesApplication {

	@Bean
	public ObjectMapper getObjectMapper(){
		return new ObjectMapper();
	}

	public static void main(String[] args) {
		SpringApplication.run(CompareLocationSubjectDataFilesApplication.class, args);
	}

}
