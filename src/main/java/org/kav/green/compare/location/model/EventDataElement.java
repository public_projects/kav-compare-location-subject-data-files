package org.kav.green.compare.location.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class EventDataElement {
    public long timestamp;
    public String identityType;
    public String identity;
    public String inception;
    public String content;
    public String contentType;
    public String provider;
    public String subjectType;
    public String subjectIdentifier;
}
