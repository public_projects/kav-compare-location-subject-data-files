package org.kav.green.compare.location.main;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.kav.green.compare.location.model.SubjectElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.*;
import java.util.stream.Collectors;


@Service
@Slf4j
public class LocationFilterProcessor {
    @Autowired
    private ObjectMapper objectMapper;

    public void filterByLocation() throws Exception {
        File inputFile = new File(
                "D:\\Users\\sm13\\Documents\\development\\kav-compare-location-subject-data-files\\compare-files-dir\\Without-Location-filter-trimmed.json");
        SubjectElement[] inputFileTypeElements = objectMapper.readValue(inputFile, SubjectElement[].class);
        log.info("original record count: {}", inputFileTypeElements.length);

        Set<String> subjectsLC = new HashSet<>();
        subjectsLC.add("LOCATION");

        approch1(Arrays.asList(inputFileTypeElements), subjectsLC);
        approch2(Arrays.asList(inputFileTypeElements), subjectsLC);
        approch2(Arrays.asList(inputFileTypeElements), new HashSet<>());

    }
    public void approch1(List<SubjectElement> inputFileTypeElements, Set<String> subjectsLC) {
        List<SubjectElement> locationFiltered1 = new ArrayList<>();
        for (SubjectElement subjectElement : inputFileTypeElements) {
            String type = subjectElement.getSubjectType();
            if (subjectsLC.contains(type)) {
                locationFiltered1.add(subjectElement);
            }
        }
        log.info("1. Filtered location count: {}", locationFiltered1.size());
    }

    public void approch2(List<SubjectElement> inputFileTypeElements, Set<String> subjectTypes) {
        List<SubjectElement> events = inputFileTypeElements;
        if (!subjectTypes.isEmpty()) {
            Set<String> subjectsLC = subjectTypes.stream().map(String::toUpperCase).collect(Collectors.toSet());
            //filter by subjectTypes
            events = events.stream()
                           .filter(event -> subjectsLC.contains(event.getSubjectType().toUpperCase()))
                           .collect(Collectors.toList());
        }

        log.info("2. Filtered location count: {}", events.size());
    }
}
