package org.kav.green.compare.location.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class SubjectElement {
    public long timeInterval;
    public String instance;
    public String agencyId;
    public String interceptId;
    public String content;
    public String type;
    public String provider;
    public String subjectType;
    public String subjectIdentifier;
}
