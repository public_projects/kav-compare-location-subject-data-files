package org.kav.green.compare.location.main;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.kav.green.compare.location.config.ConfigProperty;
import org.kav.green.compare.location.model.SubjectElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.javatuples.Pair;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Service
@Slf4j
public class FileProcessor {

    public static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");

    @Autowired
    private ConfigProperty configProperty;

    @Autowired
    private ObjectMapper objectMapper;

    public void processFiles() {
        log.info("Read files from: {}", configProperty.getLocation());

        try {
            if (!Files.isDirectory(Paths.get(configProperty.getLocation()))) {
                Files.createDirectory(Paths.get(configProperty.getLocation()));
                log.error("Given directory: {} is empty!", configProperty.getLocation());
                return;
            }

            List<File> files = Files.walk(Paths.get(configProperty.getLocation()))
                                    .filter(Files::isRegularFile)
                                    .map(Path::toFile)
                                    .collect(Collectors.toList());

            if (files.isEmpty()) {
                log.error("There is no files in directory: {}", configProperty.getLocation());
                return;
            }

            Map<String, Pair<List<String>, List<String>>> report = new HashMap<>();
            List<String> columnNames = new ArrayList<>(files.size());
            Map<String, Integer> locationCountByfile = new HashMap<>();
            for (File inputFile : files) {
                columnNames.add(inputFile.getName());
                SubjectElement[] inputFileTypeElements = objectMapper.readValue(inputFile, SubjectElement[].class);
                log.info("There are {} elements in file: {}", inputFileTypeElements.length, inputFile.getName());

                for (SubjectElement subjectElement : inputFileTypeElements) {
                    log.info("time: {}, type: {}", subjectElement.getTimeInterval(), subjectElement.getSubjectType());
                    String type = subjectElement.getSubjectType();
                    String key = null;
                    if (type.equalsIgnoreCase("LOCATION")) {
                        Instant instant = Instant.ofEpochMilli(subjectElement.getTimeInterval());
                        LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, ZoneOffset.UTC);
                        String formattedDate = localDateTime.format(formatter);
                        key = subjectElement.getTimeInterval() + ";" + formattedDate + ";;" + type;
                        incrementRecordCount(inputFile.getName(), locationCountByfile);
                    }
                    else {
                        continue;
                    }

                    Pair<List<String>, List<String>> fileNamesPair = report.get(key);
                    List<String> fileNames;
                    List<String> inputFileNames = null;
                    if (fileNamesPair == null) {
                        fileNames = new ArrayList<>();
                    }
                    else {
                        fileNames = fileNamesPair.getValue0();
                        inputFileNames = fileNamesPair.getValue1();
                    }

                    fileNames.add(inputFile.getName());
                    fileNamesPair = new Pair<>(fileNames, inputFileNames);
                    report.put(key, fileNamesPair);
                }
            }

            for (String key : report.keySet()) {
                List<String> originalDataFiles = findInputFiles(key, configProperty.getInputFileLocation());
                Pair<List<String>, List<String>> dataCollection = report.get(key);
                dataCollection = new Pair<>(dataCollection.getValue0(), originalDataFiles);
                report.put(key, dataCollection);
            }

            locationCountByfile.forEach((s, integer) -> {
                log.info("file: {}, location count: {}", s, integer);
            });

            printReport(columnNames, report, configProperty.getComparisonOutputFileLocation());
        }
        catch (IOException e) {
            log.error("Failed to create directory: {}", configProperty.getLocation(), e);
        }
    }

    public static int incrementRecordCount(String fileName, Map<String, Integer> countContainer) {
        Integer count = countContainer.get(fileName);

        if (count == null){
            count = 0;
        }

        count = count + 1;
        countContainer.put(fileName, count);
        return count;
    }

    public static List<String> findInputFiles(String key, String inputFileLocation) throws IOException {
        String recordDateStr1 = key.split(";")[0];
        String recordDateStr2 = key.split(";")[1];
        log.debug("time: {}, formattedDate: {}", recordDateStr1, recordDateStr2);

        if (!Files.isDirectory(Paths.get(inputFileLocation))) {
            Files.createDirectory(Paths.get(inputFileLocation));
            log.error("Given input file directory: {} is empty!", inputFileLocation);
            return new ArrayList<>();
        }

        List<File> files = Files.walk(Paths.get(inputFileLocation))
                                .filter(Files::isRegularFile)
                                .map(Path::toFile)
                                .collect(Collectors.toList());

        List<String> fileNames = new ArrayList<>();
        for (File file : files) {
            Files.lines(Paths.get(file.getAbsolutePath()))
                 .map(s -> s.trim())
                 .filter(s -> !s.isEmpty())
                 .filter(s -> s.contains(recordDateStr2))
                 .forEach(s -> {
                     fileNames.add(file.getName());
                     log.info("{} fround in: fileName: {} of line: [{}]", file.getName(), recordDateStr2, s);
                     return;
                 });
        }
        return fileNames;
    }

    public static void printReport(List<String> columnNames, Map<String, Pair<List<String>, List<String>>> report, String outputDirectory) {
        StringBuilder sb = new StringBuilder();
        sb.append("time;formattedDate;identityType;type;")
          .append(String.join(";", columnNames))
          .append(";")
          .append("originalDataFile")
          .append("\n");

        report.forEach((s, strings) -> {
            sb.append(s);
            for (int i = 0; i < columnNames.size(); i++) {

                List<String> recordFiles = strings.getValue0();
                if (recordFiles.contains(columnNames.get(i))) {
                    sb.append(";")
                      .append("YES");
                }
                else {
                    sb.append(";");
                }
            }

            List<String> originalFiles = strings.getValue0();
            sb.append(";")
              .append(String.join(" ", originalFiles));
            sb.append("\n");
        });

        System.err.println(sb.toString());

        String newOutputLocation = outputDirectory + File.separator + LocalDateTime.now().format(DateTimeFormatter.ofPattern(
                                                                                                                            "yyyyMMddHHmmssSSS")) +
                                   ".csv";
        writeToFile(sb.toString(), newOutputLocation);
    }

    public static void writeToFile(String jsonStr, String filePath) {
        File jsonFile = new File(filePath);
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(jsonFile));
            writer.write(jsonStr);
        }
        catch (IOException ioe) {
            log.error("Failed to write to file: {}", ioe);
        }
        finally {
            try {
                writer.close();
            }
            catch (IOException io) {
                // Just ignore
            }
        }
        log.info("Written to {}", jsonFile.getAbsolutePath());
    }
}
