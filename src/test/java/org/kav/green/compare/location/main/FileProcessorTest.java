package org.kav.green.compare.location.main;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class FileProcessorTest {

    @Autowired
    private FileProcessor fileProcessor;

    @Test
    public void testProcessFiles(){
        fileProcessor.processFiles();
    }
}
