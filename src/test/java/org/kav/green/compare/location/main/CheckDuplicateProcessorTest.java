package org.kav.green.compare.location.main;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class CheckDuplicateProcessorTest {

    @Autowired
    private CheckDuplicateProcessor checkDuplicateProcessor;

    @Test
    public void filterByLocation() throws Exception {
        checkDuplicateProcessor.filterByLocation();
    }
}
