package org.kav.green.compare.location.main;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class LocationFilterProcessorTest {

    @Autowired
    private LocationFilterProcessor locationFilterProcessor;

    @Test
    void filterByLocation() throws Exception {
        locationFilterProcessor.filterByLocation();
    }
}
